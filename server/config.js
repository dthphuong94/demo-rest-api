//=====JWT config=====
var secret = "#FPO-w0rld@secret-JwT@PBCDT#";
var exptime = "60";
// =====#END JWT config=====

//=====Database config=====
var server = "localhost";
var port = "12546";
var username = "chadmin";
var password = "fpocloud.2016";
var dbName = "childhub";

var connectionString = 'mongodb://' + username + ':' + password + '@' + server + ':' + port + '/'; // + dbName;
var worldConnectionString = 'mongodb://worldadmin:' + password + '@' + server + ':' + port + '/fpo-world'; 
// =====#END Database config=====

//=====Mail server config=====
var emailAddress = "noreply@fpo.vn";
var emailPassword = "Abcd@123";
var smtpPort = 587;
var smtpHost = 'smtp.yandex.com';
// =====#END Mail server config=====

// ===== upload config=====
var tempPhotoPath = 'C:\\FPO\\___temp-photo\\';
var videoExts = ['.mp4', '.avi', '.mpg', '.mpeg'];
// =====#END upload config=====

//=====world-service config=====
var worldUrl = 'http://202.78.227.73:2016';
var smsUrl = 'http://fpocompany.ddns.net:2000';
//=====#END world-service config=====

//=====others config=====
var maxFeedbackPerDay = 5; // maximum feedback per day
var welcomeMess = 'Welcome to ChildHub';
//=====#END others config=====

exports.NoJWTUrl = ['/', '/push'];
exports.smsUrl = smsUrl;
exports.connectionString = connectionString;
exports.worldConnectionString = worldConnectionString;
exports.secret = secret;
exports.exptime = exptime;
exports.emailAddress = emailAddress;
exports.emailPassword = emailPassword;
exports.smtpHost = smtpHost;
exports.smtpPort = smtpPort;
exports.worldUrl = worldUrl;
exports.tempPhotoPath = tempPhotoPath;
exports.videoExts = videoExts;
exports.maxFeedbackPerDay = maxFeedbackPerDay;
exports.welcomeMess = welcomeMess;