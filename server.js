var express = require('express')
var bodyParser = require('body-parser');
var app = express()
var port = process.env.PORT || 3000
var server = require('http').createServer(app)
var config = require('./server/config')
var jwt = require('./libs/JWT')
var _ = require('underscore')

app.use(bodyParser.json({limit: '5000mb'}));
app.use(bodyParser.urlencoded({limit: '5000mb', extended: true}));

app.get('/', function (req, res) {
    res.send('Hello World')

})

app.use(function(req, res, next) {
    if (_.indexOf(config.NoJWTUrl, req.originalUrl) == -1) {
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if (token) {
            console.log('Decoding token . . . ')
            var decoded = jwt.decode(token)
            if (decoded.status != 1) {
                console.log(decoded)
                return res.json({ success: false, message: 'Failed to authenticate token.' })  
            } else {
                req.decoded = decoded
                next()
            }
        } else {
            return res.status(403).send({ 
                success: false, 
                message: 'No token provided.' 
            })
        }
    } else {
        next()
    }
})

app.post('/push', function(req, res) {
    var p1 = req.body.p1
    var p2 = req.body.p2

    return res.send({
        'param-1': p1,
        'param-2': p2
    })
})

app.post('/pop', function(req, res) {
    
    console.log('---------payload----------')
    console.log(req.decoded)
    console.log('---------payload----------')
    var p1 = req.body.p1
    var p2 = req.body.p2

    return res.send({
        'param1': p1,
        'param2': p2
    })
})

server.listen(port, function() {
    console.log("Listening on port %s...", server.address().port)
});