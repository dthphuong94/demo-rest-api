const config = require(__dirname + '/../server/config');
var jwt = require('jsonwebtoken');

function encode(payload, exp) {
    // console.log(payload);
    // console.log(config.secret);
    return jwt.sign(payload, config.secret, { expiresIn: parseInt(exp) });
}

function decode(token) {
    var ret = {"status":0, "data":null, "error":null};

    try {
        ret.status = 1;
        ret.data = jwt.verify(token, config.secret);
    } catch(err) {
        ret.status = 0;
        ret.error = err.message;
    }

    return ret;
}

exports.encode = encode;
exports.decode = decode;